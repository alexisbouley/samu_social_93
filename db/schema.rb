# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_25_125719) do

  create_table "enfants", force: :cascade do |t|
    t.integer "usager_id"
    t.string "nom"
    t.string "prenom"
    t.string "sexe"
    t.date "date_naissance"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["usager_id"], name: "index_enfants_on_usager_id"
  end

  create_table "groupes", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["nom"], name: "index_groupes_on_nom", unique: true
  end

  create_table "intervenants", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "ref"
    t.index ["nom"], name: "index_intervenants_on_nom", unique: true
  end

  create_table "intervenants_maraudes", id: false, force: :cascade do |t|
    t.integer "maraude_id"
    t.integer "intervenant_id"
    t.index ["intervenant_id"], name: "index_intervenants_maraudes_on_intervenant_id"
    t.index ["maraude_id"], name: "index_intervenants_maraudes_on_maraude_id"
  end

  create_table "maraudes", force: :cascade do |t|
    t.date "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "cr"
    t.string "type_maraude"
    t.text "villes"
    t.boolean "explo"
    t.index ["date", "type_maraude"], name: "index_maraudes_on_date_and_type_maraude", unique: true
    t.index ["id"], name: "index_maraudes_on_id", unique: true
  end

  create_table "rencontres", force: :cascade do |t|
    t.integer "usager_id"
    t.date "date"
    t.string "type_renc"
    t.boolean "signale"
    t.string "signalement"
    t.text "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "prev"
    t.boolean "dnv"
    t.integer "nb_enf"
    t.string "prestas"
    t.boolean "accomp"
    t.string "type_accomp"
    t.string "ville"
    t.string "sig_contact"
    t.text "sig_coords"
    t.string "sig_structure"
    t.string "accomp_structure"
    t.boolean "tel"
    t.integer "user_id"
    t.boolean "explo"
    t.integer "nb_duvet"
    t.integer "nb_vestiaire"
    t.integer "nb_alimentaire"
    t.integer "nb_hygiene"
    t.index ["usager_id", "date", "type_renc"], name: "index_rencontres_on_usager_id_and_date_and_type_renc", unique: true
    t.index ["usager_id"], name: "index_rencontres_on_usager_id"
  end

  create_table "structures", force: :cascade do |t|
    t.string "nom"
    t.string "ville"
    t.string "adresse"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["nom"], name: "index_structures_on_nom", unique: true
  end

  create_table "structures_usagers", id: false, force: :cascade do |t|
    t.integer "structure_id"
    t.integer "usager_id"
    t.index ["structure_id"], name: "index_structures_usagers_on_structure_id"
    t.index ["usager_id"], name: "index_structures_usagers_on_usager_id"
  end

  create_table "type_rencs", force: :cascade do |t|
    t.string "nom"
    t.boolean "mar"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "usagers", force: :cascade do |t|
    t.string "nom"
    t.string "prenom"
    t.string "sexe"
    t.string "ville"
    t.date "date_naissance"
    t.string "tel"
    t.text "notes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "adresse"
    t.string "adresse_précis"
    t.string "user_id"
    t.boolean "pqi"
    t.string "pqi_histo"
    t.text "fiche"
    t.integer "groupe_id"
    t.text "ressources"
    t.float "montant"
    t.text "fiche_jour"
    t.boolean "dom"
    t.string "dom_org"
    t.string "dom_adr"
    t.boolean "tut"
    t.boolean "cur"
    t.string "tutcur_org"
    t.boolean "suivi"
    t.string "suivi_org"
    t.boolean "sejour"
    t.boolean "cfr"
    t.date "carte_date"
    t.text "autres_infos"
    t.string "prestas_med"
    t.string "medecin"
    t.text "medecin_infos"
    t.string "pb_sante"
    t.text "infos_sante"
    t.boolean "dmde"
    t.date "date_dmde"
    t.boolean "vu"
    t.string "ref"
    t.boolean "mobil"
    t.string "nom2"
    t.string "prenom2"
    t.string "sexe2"
    t.string "tel2"
    t.date "date_naissance2"
    t.boolean "couple"
    t.boolean "enceinte"
    t.boolean "enceinte2"
    t.index ["groupe_id"], name: "index_usagers_on_groupe_id"
    t.index ["nom"], name: "index_usagers_on_nom"
    t.index ["ville"], name: "index_usagers_on_ville"
  end

  create_table "users", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "prenom"
    t.string "identifiant"
    t.string "password_digest"
    t.boolean "admin", default: false
    t.boolean "benev"
    t.date "date_naissance"
    t.index ["identifiant"], name: "index_users_on_identifiant", unique: true
  end

  create_table "villes", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "ville_93"
    t.index ["nom"], name: "index_villes_on_nom", unique: true
  end

  add_foreign_key "enfants", "usagers"
  add_foreign_key "rencontres", "usagers"
  add_foreign_key "usagers", "groupes"
end
