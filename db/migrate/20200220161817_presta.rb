class Presta < ActiveRecord::Migration[6.0]
  def change

  	add_column :rencontres, :nb_duvet, :integer
  	add_column :rencontres, :nb_vestiaire, :integer
  	add_column :rencontres, :nb_alimentaire, :integer
  	add_column :rencontres, :nb_hygiene, :integer
  end
end
