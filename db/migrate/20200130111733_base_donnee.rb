class BaseDonnee < ActiveRecord::Migration[6.0]
  def change
  	create_table :users do |t|
      t.string :nom

      t.timestamps null: false
    end

    add_column :users, :prenom, :string
    add_column :users, :identifiant, :string
    add_column :users, :password_digest, :string
    add_index :users, :identifiant, unique: true
    add_column :users, :admin, :boolean, default: false


    create_table :usagers do |t|
      t.string :nom
      t.string :prenom
      t.string :sexe
      t.string :ville
      t.date :date_naissance
      t.string :tel
      t.text :notes

      t.timestamps null: false
    end

    add_column :usagers, :adresse, :string
    add_column :usagers, :adresse_précis, :string
    add_column :usagers, :user_id, :string
    add_column :usagers, :pqi, :boolean
    add_column :usagers, :pqi_histo, :string
    add_index :usagers, :nom
  	add_index :usagers, :ville

  	create_table :maraudes do |t|
      t.date :date

      t.timestamps null: false
    end

    add_column :maraudes, :cr, :text
    add_column :maraudes, :type_maraude, :string
    add_column :maraudes, :villes, :text

    add_column :usagers, :fiche, :text

    create_table :rencontres do |t|
      t.references :usager, index: true, foreign_key: true
      t.date :date
      t.string :type_renc
      t.boolean :signale
      t.string :signalement
      t.text :details

      t.timestamps null: false
    end

    add_index :rencontres, [:usager_id, :date, :type_renc], :unique => true
    add_column :rencontres, :prev, :boolean
    add_column :rencontres, :dnv, :boolean
    add_column :rencontres, :nb_enf, :integer
    add_column :rencontres, :prestas, :string
    add_column :rencontres, :accomp, :boolean
    add_column :rencontres, :type_accomp, :string

    create_table :enfants do |t|
      t.references :usager, index: true, foreign_key: true
      t.string :nom
      t.string :prenom
      t.string :sexe
      t.date :date_naissance

      t.timestamps null: false
    end

    create_table :groupes do |t|
      t.string :nom

      t.timestamps null: false
    end

    add_reference :usagers, :groupe, index: true, foreign_key: true
  	add_column :usagers, :ressources, :text
    add_column :usagers, :montant, :float
    add_column :usagers, :fiche_jour, :text
    add_column :usagers, :dom, :boolean
    add_column :usagers, :dom_org, :string
    add_column :usagers, :dom_adr, :string
    add_column :usagers, :tut, :boolean
    add_column :usagers, :cur, :boolean
    add_column :usagers, :tutcur_org, :string
    add_column :usagers, :suivi, :boolean
    add_column :usagers, :suivi_org, :string
    add_column :usagers, :sejour, :boolean
    add_column :usagers, :cfr, :boolean
    add_column :usagers, :carte_date, :date
    add_column :usagers, :autres_infos, :text
    add_column :usagers, :prestas_med, :string
    add_column :usagers, :medecin, :string
    add_column :usagers, :medecin_infos, :text
    add_column :usagers, :pb_sante, :string
    add_column :usagers, :infos_sante, :text

    add_index :maraudes, :id, unique: true
  	add_index :maraudes, [:date, :type_maraude], unique: true

  	add_column :users, :benev, :boolean
  	add_column :rencontres, :ville, :string

  	add_column :rencontres, :sig_contact, :string
    add_column :rencontres, :sig_coords, :text

    add_column :rencontres, :sig_structure, :string
    add_column :rencontres, :accomp_structure, :string

    add_column :usagers, :dmde, :boolean
    add_column :usagers, :date_dmde, :date

    add_column :rencontres, :tel, :boolean

    add_column :rencontres, :user_id, :integer

    add_column :usagers, :vu, :boolean

    create_table :intervenants do |t|
      t.string :nom

      t.timestamps null: false
    end

    create_table :intervenants_maraudes, id: false do |t|
    	t.belongs_to :maraude, index: true
    	t.belongs_to :intervenant, index: true
    end

    add_index :groupes, :nom, unique: true
  	add_index :intervenants, :nom, unique: true

  	add_column :intervenants, :ref, :boolean
    add_column :usagers, :ref, :string
    add_column :usagers, :mobil, :boolean

    create_table :structures do |t|
      t.string :nom
      t.string :ville
      t.string :adresse

      t.timestamps null: false
    end

    add_index :structures, :nom, unique: true

    create_table :structures_usagers, id: false do |t|
    	t.belongs_to :structure, index: true
    	t.belongs_to :usager, index: true
    end

    create_table :villes do |t|
      t.string :nom

      t.timestamps null: false
    end

    add_index :villes, :nom, unique: true
    add_column :villes, :ville_93, :boolean

    create_table :type_rencs do |t|
      t.string :nom
      t.boolean :mar

      t.timestamps null: false
    end

    add_column :maraudes, :explo, :boolean

    add_column :usagers, :nom2, :string
  	add_column :usagers, :prenom2, :string
  	add_column :usagers, :sexe2, :string
  	add_column :usagers, :tel2, :string
  	add_column :usagers, :date_naissance2, :date
  	add_column :usagers, :couple, :boolean
  	add_column :usagers, :enceinte, :boolean
  	add_column :usagers, :enceinte2, :boolean

  	add_column :rencontres, :explo, :boolean
  end
end
