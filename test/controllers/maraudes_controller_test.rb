require 'test_helper'

class MaraudesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get maraudes_index_url
    assert_response :success
  end

  test "should get show" do
    get maraudes_show_url
    assert_response :success
  end

  test "should get new" do
    get maraudes_new_url
    assert_response :success
  end

  test "should get create" do
    get maraudes_create_url
    assert_response :success
  end

  test "should get destroy" do
    get maraudes_destroy_url
    assert_response :success
  end

  test "should get edit" do
    get maraudes_edit_url
    assert_response :success
  end

  test "should get update" do
    get maraudes_update_url
    assert_response :success
  end

end
