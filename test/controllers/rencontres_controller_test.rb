require 'test_helper'

class RencontresControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get rencontres_new_url
    assert_response :success
  end

  test "should get new_groupe" do
    get rencontres_new_groupe_url
    assert_response :success
  end

  test "should get edit_form" do
    get rencontres_edit_form_url
    assert_response :success
  end

  test "should get edit" do
    get rencontres_edit_url
    assert_response :success
  end

end
