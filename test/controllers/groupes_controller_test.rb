require 'test_helper'

class GroupesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get groupes_index_url
    assert_response :success
  end

  test "should get show" do
    get groupes_show_url
    assert_response :success
  end

  test "should get edit" do
    get groupes_edit_url
    assert_response :success
  end

  test "should get update" do
    get groupes_update_url
    assert_response :success
  end

  test "should get destroy" do
    get groupes_destroy_url
    assert_response :success
  end

end
