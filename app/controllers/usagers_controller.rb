class UsagersController < ApplicationController

	  before_action :logged_in_user,  only: [:index, :pqi, :new, :create, :edit_comp, :post_comp, :edit, :fiche, :fiche_jour, :update, :show, :destroy]
    before_action :benev_user,      only: [:edit_comp, :post_comp, :edit, :fiche, :fiche_jour, :update]
    before_action :correct_user,    only: [:destroy]

  	def index
  		if params[:search]
      	@usagers = Usager.search(params[:search]).paginate(page: params[:page], per_page: 50)
      	if @usagers.empty?
        	@usagers = Usager.search(params[:search].split('/').reverse.join('-')).paginate(page: params[:page], per_page: 50)
      	end
    	else
     		@usagers = Usager.paginate(page: params[:page], per_page: 50)
    	end
   
    	if params[:ordre_de_tri] == "Ordre chronologique de modification" 
		    @usagers = @usagers.order('updated_at DESC')
		    @proposition=["Ordre chronologique de modification", "Ordre alphabétique"]		    
		  else
		    @usagers= @usagers.order('nom ASC').order('prenom ASC') 
		    @proposition=["Ordre alphabétique", "Ordre chronologique de modification"]
		  end
    end

    def pqi
      @villes = []
      Ville.where(ville_93: true).order('nom ASC').each do |v|
        @villes << "#{v.nom}"
      end

      if params[:ordre_de_tri] == "Ordre géographique"
        @usagers = Usager.where(pqi: true, ville: params[:ville]).order('adresse ASC, adresse_précis ASC, nom ASC') unless not params[:ville]
        @proposition = ["Ordre géographique", "Ordre alphabétique"]
      else
        @usagers = Usager.where(pqi: true, ville: params[:ville]).order('nom ASC, prenom ASC') unless not params[:ville]
        @proposition = ["Ordre alphabétique", "Ordre géographique"]
      end

      @ville_actuelle = params[:ville]
    end


    def new
      @villes = []
      Ville.order("ville_93 DESC").order('nom ASC').each do |v|
        @villes << ["#{v.nom}"]
      end
    end

    def create

      @usager = Usager.new(usager_params)

      @usager.user_id = @current_user.id

      if @usager.nom
        @usager.nom.upcase!
      end
      if @usager.nom2
        @usager.nom2.upcase!
      end
      @usager.enfants.each do |enf|
        if enf.nom
          enf.nom.upcase!
        end
      end

      if @usager.enceinte
        if @usager.sexe == "Mr"
          @usager.enceinte = false
        end
      end

      if @usager.enceinte2
        if @usager.sexe2 == "Mr"
          @usager.enceinte2 = false
        end
      end

      if @usager.save
        if @usager.ref.present?
          interv = Intervenant.find_or_create_by(nom: @usager.ref.split(' ').map(&:capitalize).join(' ').split('-').map(&:capitalize).join('-'))
          interv.ref = true
          interv.save
        end
        if @usager.pqi
          @usager.pqi_histo = ""
          @usager.pqi_histo << Date.today.strftime("%d/%m/%y")
          @usager.save
        end
        if @usager.dmde && (@usager.date_dmde==nil)
          @usager.update(date_dmde: @usager.created_at)
        end

        flash[:success] = "Nouvel usager ajouté !"
        if @current_user.benev?
          redirect_to "/usagers/#{@usager.id}"
        else
          redirect_to "/usagers/edit_comp/#{@usager.id}"
        end

      else
        flash[:danger] = "Ajoutez au moins le prénom ou le nom de chaque usager ainsi que leur sexe et la ville !"
        redirect_to '/usagers/new'
      end
    end

    def edit_comp
      @usager = Usager.find(params[:id])
      @ressources = [ ["Salaire", "Salaire"],
                    ["Retraite", "Retraite"],
                    ["RSA", "RSA"],
                    ["AAH", "AAH"],
                    ["Prestation(s) familiale(s)", "Prestation(s) familiale(s)"],
                    ["ATA", "ATA"],
                    ["ASS", "ASS"],
                    ["ARE", "ARE"],
                    ["Rémunération de formation", "Rémunération de formation"],
                    ["Pension d'invalidité", "Pension d'invalidité"],
                    ["Autre", "Autre"],
                    ["Sans ressources", "Sans ressources"]]
      @prestas_med = [["CMU", "CMU"],
                      ["CMUC", "CMUC"],
                      ["AME", "AME"],
                      ["Régime général", "Régime général"]]
    end


    def post_comp
      @usager = Usager.find(params[:id])
      params[:usager][:ressources] = params[:usager][:ressources].reject{ |a| a == '0' }.join("\n")
      params[:usager][:prestas_med] = params[:usager][:prestas_med].reject{ |a| a == '0' }.join("\n")
      
      if @usager.update(comp_params)
        flash[:success] = "Informations complémentaires éditées"
        if Usager.maximum("id") == @usager.id
          redirect_to "/rencontres/new/#{@usager.id}"
        else
          redirect_to "/usagers/#{@usager.id}"
        end
      else
        flash[:danger] = "Problème inconnu, veuillez réessayer"
        redirect_to "/usagers/edit_comp/#{@usager.id}"
      end     
    end

    def edit
      session[:stored] = "edit"

      @villes = [["Ville inconnue"], ["Autre (hors 93)"]]
      Ville.where(ville_93: true).order('nom ASC').each do |v|
        @villes << ["#{v.nom}"]
      end

      @usager = Usager.find(params[:id])
    end

    def fiche
      @usager = Usager.find(params[:id])
      session[:stored] = "fiche"
    end

    def fiche_jour
      @usager = Usager.find(params[:id])
      session[:stored] = "fiche_jour"
    end

    def update
      @usager = Usager.find(params[:id])
      statut = session[:stored]
      session.delete(:stored)

      if statut == "edit"

        stored_pqi = @usager.pqi

        if @usager.update(usager_params)
          if @usager.ref.present?
            interv = Intervenant.find_or_create_by(nom: @usager.ref.split(' ').map(&:capitalize).join(' ').split('-').map(&:capitalize).join('-'))
            interv.ref = true
            interv.save
          end

          if @usager.dmde && (@usager.date_dmde==nil)
            @usager.update(date_dmde: @usager.updated_at)
          end

          if @usager.pqi != stored_pqi
            if @usager.pqi
              if @usager.pqi_histo
                @usager.pqi_histo << " ///// "
              else
                @usager.pqi_histo = ""
              end
            else
              @usager.pqi_histo << " - " unless @usager.pqi_histo.nil?
            end
            arr = @usager.pqi_histo.split(' ///// ')
            if arr.last == "#{Date.today.strftime("%d/%m/%y")} - "
              if arr.length >= 2
                @usager.pqi_histo = arr[0..(arr.length-2)].join(' ///// ')
              else
                @usager.pqi_histo = nil
              end
            else
              @usager.pqi_histo << Date.today.strftime("%d/%m/%y") unless @usager.pqi_histo.nil?
            end
          end

          if @usager.nom
            @usager.nom.upcase!
          end
          if @usager.nom2
            @usager.nom2.upcase!
          end
          @usager.enfants.each do |enf|
            if enf.nom
              enf.nom.upcase!
            end
          end

          if @usager.enceinte
            if @usager.sexe == "Mr"
              @usager.enceinte = false
            end
          end

          if @usager.enceinte2
            if @usager.sexe2 == "Mr"
              @usager.enceinte2 = false
            end
          end

          if params[:usager][:groupe_nom] == ""
            @usager.update(groupe_id: nil)
          end

          @usager.save

          flash[:success] = "Usager édité"
          redirect_to "/usagers/#{@usager.id}"

        else 
          flash[:danger] = "Mise à jour impossible. Veillez à remplir les informations nécessaires (Nom et/ou prénom, ville et sexe)."
          redirect_to "/usagers/edit/#{@usager.id}"
        end

      elsif statut == "fiche"

        @usager.update(fiche: params[:usager][:fiche])
        flash[:success] = "Fiche de rencontres usager éditée"
        redirect_to "/usagers/#{@usager.id}"

      elsif statut == "fiche_jour"

        @usager.update(fiche_jour: params[:usager][:fiche_jour])
        flash[:success] = "Fiche de suivi jour usager éditée"
        redirect_to "/usagers/#{@usager.id}"

      end
    end

    def show
      @usager = Usager.find(params[:id])
    end

    def destroy
      Usager.find(params[:id]).destroy
      flash[:success] = "Usager supprimé"
      redirect_to usagers_path
    end

    private

    def correct_user
      @usager = Usager.find(params[:id])
      redirect_back(fallback_location: root_path) unless  ("#{@current_user.id}" == @usager.user_id || @current_user.admin?)
    end
      
    def usager_params
      params.require(:usager).permit( :nom,
                                      :prenom,
                                      :sexe,
                                      :enceinte,
                                      :ville,
                                      :adresse,
                                      :adresse_précis,
                                      :date_naissance,
                                      :tel,
                                      :notes,
                                      :pqi,
                                      :pqi_histo,
                                      :fiche,
                                      :groupe_nom,
                                      :ressources,
                                      :montant,
                                      :ref,
                                      :dmde,
                                      :date_dmde,
                                      :vu,
                                      :couple,
                                      :nom2,
                                      :prenom2,
                                      :sexe2,
                                      :date_naissance2,
                                      :tel2,
                                      :enceinte2,
                                      enfants_attributes: [ :id,
                                                            :nom,
                                                            :prenom,
                                                            :sexe,
                                                            :date_naissance,
                                                            :_destroy ])
    end

    def comp_params
      params.require(:usager).permit( :montant,
                                      :dom,
                                      :dom_org,
                                      :dom_adr,
                                      :tut,
                                      :cur,
                                      :tutcur_org,
                                      :suivi,
                                      :suivi_org,
                                      :sejour,
                                      :cfr,
                                      :carte_date,
                                      :medecin,
                                      :medecin_infos,
                                      :pb_sante,
                                      :mobil,
                                      :infos_sante,
                                      :autres_infos,
                                      :ressources,
                                      :prestas_med)
    end

end
