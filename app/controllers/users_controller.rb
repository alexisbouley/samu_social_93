class UsersController < ApplicationController

  before_action :logged_in_user,  only: [:new, :create, :index, :show, :edit, :update, :destroy]
  before_action :correct_user,    only: [:edit, :update]
  before_action :admin_user,      only: [:new, :create, :destroy]

  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end

  def show
    @user = User.find(params[:id])
  end

  def new
  end

  def create
   
    if params[:user][:password_digest] == params[:user][:password_confirmation] && (params[:user][:benev] != "1" || "1" != params[:user][:admin])
      @user = User.new(user_params)
      if @user.save
        flash[:success] = "Profil édité"
        redirect_to "/users/show/#{@user.id}"
      else
        flash[:danger] = "Profil non édité. Assurez vous d'avoir un nom, un prénom, un identifiant unique d'au moins 4 caractères et un mot de passe d'au moins 6 caractères."
        redirect_to "/signup"
      end

    elsif params[:user][:password_digest] != params[:user][:password_confirmation]
      flash[:danger] = "Veuillez taper deux fois le même mot de passe."
      redirect_to "/signup"

    else
      flash[:danger] = "Un bénévole ne peut pas être administrateur."
      redirect_to "/signup"
    end

  end

  def edit
    @user = User.find(params[:id])
  end

  def update

    @user = User.find(params[:id])

    if params[:user][:password_digest] == params[:user][:password_confirmation] && (params[:user][:benev] != "1" || "1" != params[:user][:admin])
    
      if @user.update(user_params)
        flash[:success] = "Profil édité"
        redirect_to "/users/show/#{@user.id}"
      else
        flash[:danger] = "Profil non édité. Assurez vous d'avoir un nom, un prénom, un identifiant unique d'au moins 4 caractères et un mot de passe d'au moins 6 caractères."
        redirect_to "/users/edit/#{@user.id}"   
      end 

    elsif params[:user][:password_digest] != params[:user][:password_confirmation]
      flash[:danger] = "Veuillez taper deux fois le même mot de passe."
      redirect_to "/users/edit/#{@user.id}"

    else 
      flash[:danger] = "Un bénévole ne peut pas être administrateur."
      redirect_to "/users/edit/#{@user.id}"
    end

  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Utilisateur supprimé"
    redirect_to users_path
  end

  private 

    def user_params
      params.require(:user).permit(:nom, :prenom, :identifiant, :password_digest, :benev, :admin, :date_naissance)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_back(fallback_location: root_path) unless  (@current_user == @user || @current_user.admin?)
    end

end
