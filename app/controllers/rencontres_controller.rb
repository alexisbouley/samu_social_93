class RencontresController < ApplicationController

  before_action :logged_in_user, only: [:new, :create, :new_groupe, :post_groupe, :edit_form, :post_form, :edit, :update]
  
  def new

    @signalements = [ ["Signalement 115", "Signalement 115"],
                      ["Signalement tiers", "Signalement tiers"],
                      ["Croisé(e) en maraude", "Croisé(e) en maraude"]]
    @types = []
    unless @current_user.benev?
      TypeRenc.all.each do |t|
        @types << ["#{t.nom}"]
      end
    else
      @types << ["Maraude bénévole"]
    end

    @accomps = [["Accompagnement 115", "Accompagnement 115"],
                ["Accompagnement SIAO", "Accompagnement SIAO"],
                ["Autre", "Autre"]]

    @villes = []
    Ville.all.order('nom ASC').each do |v|
      @villes << ["#{v.nom}"]
    end

    @usager = Usager.find(params[:id])

    @nb_enf = []
    i = 0
    until i == @usager.enfants.count + 1 do
      @nb_enf << ["#{i}", "#{i}"]
      i += 1
    end

    @prop = [["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"]]
    
    @rencontre = @usager.rencontres.build
  end


  def create

  	@usager = Usager.find(params[:id])
  	@rencontre = @usager.rencontres.build(rencontre_params)
  	@rencontre.user_id = @current_user.id

  	if @current_user.benev?
      @rencontre.prev = false
    end

    if session.has_key?('groupe')
      @rencontre.date = session[:date]
      @rencontre.type_renc = session[:type_renc]
      @rencontre.ville = session[:ville]
    end

    if @rencontre.valid?

    	if TypeRenc.find_by(nom: @rencontre.type_renc).mar? && !@rencontre.prev     
        	if !Maraude.find_by(date: @rencontre.date, type_maraude: @rencontre.type_renc).present?
          		@maraude = Maraude.create(date: @rencontre.date, type_maraude: @rencontre.type_renc, villes: "", explo: false)
        	end      
     	end

     	#on edite la fiche de l'usager et peut être la fiche jour de l'usager 
     	rencontre_u = "// #{@rencontre.type_renc} [#{@rencontre.date.strftime("%d/%m/%y")}] //"

	    if @rencontre.signale && !@rencontre.signalement.empty?
	        rencontre_u << "\n#{@rencontre.signalement}"
	        if @rencontre.signalement == "Signalement tiers" && @rencontre.sig_contact.present?
	          rencontre_u << " (Contact : #{@rencontre.sig_contact}"
	          rencontre_u << " / Coordonnées : #{@rencontre.sig_coords}" unless @rencontre.sig_coords.empty?
	          rencontre_u << ")"
	        end
	    end

	    if @rencontre.accomp && !@rencontre.type_accomp.empty?
	       rencontre_u << "\n#{@rencontre.type_accomp}"
	    end

	    rencontre_p = ""

      if @rencontre.nb_alimentaire > 0
        rencontre_p << "#{@rencontre.nb_alimentaire} #{"prestation".pluralize(@rencontre.nb_alimentaire)} alimentaire"
      end
      if @rencontre.nb_vestiaire > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_vestiaire} #{"prestation".pluralize(@rencontre.nb_vestiaire)} vestiaire"
      end
      if @rencontre.nb_hygiene > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_hygiene} #{"prestation".pluralize(@rencontre.nb_hygiene)} hygiene"
      end
      if @rencontre.nb_duvet > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_duvet} #{"prestation".pluralize(@rencontre.nb_duvet)} duvet"
      end

	    if rencontre_p && !rencontre_p.empty?
	       rencontre_u << "\n#{rencontre_p}"
	    end

	    if @rencontre.dnv	        
	       rencontre_u << "\nMaraude déplacée mais personne non vue."
	    end

	    if @rencontre.tel
	       rencontre_u << "\nJoint au téléphone."
	    end

	    if @usager.enfants.any? && !@rencontre.dnv
	        rencontre_u << "\nAvec #{@rencontre.nb_enf} #{"enfant".pluralize(@rencontre.nb_enf)}."
	    end

	    if !@rencontre.details.empty?
	        rencontre_u << "\n#{@rencontre.details}"
	    else
	       rencontre_u << "\nRencontre sans détails."
	    end

	    if @rencontre.type_renc == "Rencontre pôle jour"
	    	rencontre_u2 = rencontre_u.chop
	    	rencontre_u2 << rencontre_u.last
	    	rencontre_u2 << "\n\n\n" unless !@usager.fiche_jour
	    	rencontre_u2 << "#{@usager.fiche_jour}"
	    	u_fiche_jour = rencontre_u2
	    end

	    rencontre_u << "\n\n\n" unless !@usager.fiche
	    rencontre_u << "#{@usager.fiche}"	     
	    u_fiche = rencontre_u

	    #on sauvegarde tout

	    @usager.fiche = u_fiche if u_fiche
	    @usager.fiche_jour = u_fiche_jour if u_fiche_jour
	    @usager.save

	    @rencontre.save

	    if TypeRenc.find_by(nom: @rencontre.type_renc).mar?
          m = Maraude.where(date: @rencontre.date, type_maraude: @rencontre.type_renc).first
          @rencontre.update(explo: m.explo)
        else
          @rencontre.update(explo: false)
        end

        if @usager.enfants.any? && @usager.couple
      		flash[:success] = "Rencontre ajoutée avec Famille #{@usager.sexe} #{@usager.nom} #{@usager.prenom} et #{@usager.sexe2} #{@usager.nom2} #{@usager.prenom2}"
     	  elsif @usager.enfants.any? && (not @usager.couple)
       		flash[:success] = "Rencontre ajoutée avec Famille #{@usager.sexe} #{@usager.nom} #{@usager.prenom}"
    	  elsif (not @usager.enfants.any?) && @usager.couple
       		flash[:success] = "Rencontre ajoutée avec Couple #{@usager.sexe} #{@usager.nom} #{@usager.prenom} et #{@usager.sexe2} #{@usager.nom2} #{@usager.prenom2}"
    	  else
      		flash[:success] = "Rencontre ajoutée avec #{@usager.sexe} #{@usager.nom} #{@usager.prenom}"
    	  end

        if session.has_key?('groupe')
          if session[:usagers_ids].empty?
            session.delete(:groupe)
            session.delete(:usagers_ids)
            session.delete(:type_renc)
            session.delete(:date)
            session.delete(:ville)
            redirect_to "/pqi/#{@rencontre.ville}"
          else
            redirect_to "/rencontres/new/#{session[:usagers_ids].pop}"
          end

        else
          if @usager.groupe && @usager.groupe.usagers.count > 1 
            session[:type_renc] = @rencontre.type_renc
            session[:date] = @rencontre.date
            session[:ville] = @rencontre.ville
            redirect_to "/rencontres/groupe/#{@usager.id}"
          else
            redirect_to "/pqi/#{@rencontre.ville}"
          end  
        end

    else
    	if params[:rencontre][:date].blank?
      		flash[:danger] = "Renseignez une date"
      	end
    	if params[:rencontre][:type_renc].empty?
      		flash[:danger] = "Précisez un type de rencontre"
      	end

    	if params[:rencontre][:ville].empty?
      		flash[:danger] = "Précisez une ville de rencontre"
      	end

      	if Rencontre.where(usager_id: @usager.id, date: params[:rencontre][:date], type_renc: params[:rencontre][:type_renc]).count > 0
      		flash[:danger] = "Cette rencontre existe déjà"
      	end

      	redirect_to "/rencontres/new/#{@usager.id}"
    end

  end

  def new_groupe
    @usager = Usager.find(params[:id])
    @groupe = @usager.groupe
    @usagers = @groupe.usagers.select{ |usager| usager.id != @usager.id }   
  end

  def post_groupe
    u = Usager.find(params[:id])
    session[:usagers_ids] = params[:rencontre][:usagers].reject{ |a| a == '0' }
    session[:groupe] = true
    if session[:usagers_ids].empty?
      @ville_de_la_rencontre=Rencontre.where(usager_id: u.id).reorder("updated_at DESC").first.ville
      session.delete(:groupe)
      session.delete(:usagers_ids)
      session.delete(:type_renc)
      session.delete(:date)
      session.delete(:ville)
      redirect_to "/pqi/#{@ville_de_la_rencontre}"
    else
      redirect_to "/rencontres/new/#{session[:usagers_ids].pop}"
    end

  end

  def edit_form

  	@types = []
    unless @current_user.benev?
      TypeRenc.all.each do |t|
        @types << ["#{t.nom}"]
      end
    else
      @types << ["Maraude bénévole"]
    end

    @usager = Usager.find(params[:id])

  end


  def post_form
    
    @usager = Usager.find(params[:id])
    r = Rencontre.find_by(usager_id: @usager.id,
                          date: params[:rencontres][:date].to_date.strftime("%F"),
                          type_renc: params[:rencontres][:type_renc])

   	if r.nil?
   		flash[:danger] = "Cette rencontre n'a pas été trouvée"
        redirect_to "/rencontres/edit_form/#{@usager.id}"
    
    elsif params[:suppr_renc]
         r.destroy
         if @usager.fiche.present?
           	u_del = @usager.fiche.split("//")
           	rank = u_del.index(" #{r.type_renc} [#{r.date.strftime("%d/%m/%y")}] ")
            if rank
              while rank != nil
                u_del = u_del[0..(rank-1)].concat(u_del[(rank+2)..(u_del.length-1)])
                rank = u_del.index(" #{r.type_renc} [#{r.date.strftime("%d/%m/%y")}] ")
              end
            end
            @usager.fiche = "#{u_del.join("//")}"
        end

        if @usager.fiche_jour.present? && r.type_renc == "Rencontre pôle jour"
            u_del2 = @usager.fiche_jour.split("//")
            rank2 = u_del2.index(" #{r.type_renc} [#{r.date.strftime("%d/%m/%y")}] ")
            if rank2
              while rank2 != nil
                u_del2 = u_del2[0..(rank2-1)].concat(u_del2[(rank2+2)..(u_del2.length-1)])
                rank2 = u_del2.index(" #{r.type_renc} [#{r.date.strftime("%d/%m/%y")}] ")
              end
            end
            @usager.fiche_jour = "#{u_del2.join("//")}"
        end

        @usager.save

        flash[:success] = "Rencontre supprimée (Vérifiez que la rencontre a bien été retirée dans la fiche suivi de l'usager)"
        redirect_to "/usagers/#{@usager.id}"
   
    elsif params[:edit_renc]
        redirect_to "/rencontres/edit/#{r.id}"
    end

  end

  def edit

    @rencontre = Rencontre.find(params[:id])
    @usager = Usager.find(@rencontre.usager_id)
   
    @nb_enf = []
    i = 0
    until i == @usager.enfants.count + 1 do
      @nb_enf << ["#{i}", "#{i}"]
      i += 1
    end

    @signalements = [ ["Signalement 115", "Signalement 115"],
                      ["Signalement tiers", "Signalement tiers"],
                      ["Croisé(e) en maraude", "Croisé(e) en maraude"]]                      
    
    @types = []
    unless @current_user.benev?
      TypeRenc.all.each do |t|
        @types << ["#{t.nom}"]
      end
    else
      @types << ["Maraude bénévole"]
    end

    @accomps = [["Accompagnement 115", "Accompagnement 115"],
                  ["Accompagnement SIAO", "Accompagnement SIAO"],
                  ["Autre", "Autre"]]

    @prop = [["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"]]

    @villes = []
    Ville.all.order('nom ASC').each do |v|
        @villes << ["#{v.nom}"]
    end
     
  end

  def update

  	@rencontre = Rencontre.find(params[:id])
  	@usager = Usager.find(@rencontre.usager_id)
  	ex_type_renc = @rencontre.type_renc
  	ex_date = @rencontre.date.strftime("%d/%m/%y")

    if @rencontre.update(rencontre_params)

    	if @current_user.benev?
          @rencontre.update(prev: false)
        end

    	if TypeRenc.find_by(nom: @rencontre.type_renc).mar? && !@rencontre.prev     
        	if !Maraude.find_by(date: @rencontre.date, type_maraude: @rencontre.type_renc).present?
          		@maraude = Maraude.create(date: @rencontre.date, type_maraude: @rencontre.type_renc, villes: "", explo: false)
        	end      
     	end

     	#on edite la fiche de l'usager et peut être la fiche jour de l'usager 
     	rencontre_u = "// #{@rencontre.type_renc} [#{@rencontre.date.strftime("%d/%m/%y")}] //"

	    if @rencontre.signale && !@rencontre.signalement.empty?
	        rencontre_u << "\n#{@rencontre.signalement}"
	        if @rencontre.signalement == "Signalement tiers" && @rencontre.sig_contact.present?
	          rencontre_u << " (Contact : #{@rencontre.sig_contact}"
	          rencontre_u << " / Coordonnées : #{@rencontre.sig_coords}" unless @rencontre.sig_coords.empty?
	          rencontre_u << ")"
	        end
	    end

	    if @rencontre.accomp && !@rencontre.type_accomp.empty?
	       rencontre_u << "\n#{@rencontre.type_accomp}"
	    end

	    rencontre_p = ""

      if @rencontre.nb_alimentaire > 0
        rencontre_p << "#{@rencontre.nb_alimentaire} #{"prestation".pluralize(@rencontre.nb_alimentaire)} alimentaire"
      end
      if @rencontre.nb_vestiaire > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_vestiaire} #{"prestation".pluralize(@rencontre.nb_vestiaire)} vestiaire"
      end
      if @rencontre.nb_hygiene > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_hygiene} #{"prestation".pluralize(@rencontre.nb_hygiene)} hygiene"
      end
      if @rencontre.nb_duvet > 0
        if !rencontre_p.empty?
          rencontre_p << " - "
        end
        rencontre_p << "#{@rencontre.nb_duvet} #{"prestation".pluralize(@rencontre.nb_duvet)} duvet"
      end

      if rencontre_p && !rencontre_p.empty?
         rencontre_u << "\n#{rencontre_p}"
      end

	    if @rencontre.dnv	        
	       rencontre_u << "\nMaraude déplacée mais personne non vue."
	    end

	    if @rencontre.tel
	       rencontre_u << "\nJoint au téléphone."
	    end

	    if @usager.enfants.any? && !@rencontre.dnv
	        rencontre_u << "\nAvec #{@rencontre.nb_enf} #{"enfant".pluralize(@rencontre.nb_enf)}."
	    end

	    if !@rencontre.details.empty?
	        rencontre_u << "\n#{@rencontre.details}"
	    else
	       rencontre_u << "\nRencontre sans détails."
	    end


	    if @rencontre.type_renc == "Rencontre pôle jour"
	    	rencontre_u2 = rencontre_u.chop
	    	rencontre_u2 << rencontre_u.last
	    	rencontre_u2 << "\n\n\n" unless !@usager.fiche_jour
	    	if @usager.fiche_jour.present?
            	u_del = @usager.fiche_jour.split("//")
            	rank = u_del.index(" #{ex_type_renc} [#{ex_date}] ")
            	if rank
              		while rank != nil
                		u_del = u_del[0..(rank-1)].concat(u_del[(rank+2)..(u_del.length-1)])
                		rank = u_del.index(" #{ex_type_renc} [#{ex_date}] ")
              		end
            	end
            	rencontre_u2 << "#{u_del.join("//")}"
          	end
	    	u_fiche_jour = rencontre_u2
	    end

	    rencontre_u << "\n\n\n" unless !@usager.fiche     
	    if @usager.fiche.present?
            u_del = @usager.fiche.split("//")
            rank = u_del.index(" #{ex_type_renc} [#{ex_date}] ")
            if rank
              while rank != nil
                u_del = u_del[0..(rank-1)].concat(u_del[(rank+2)..(u_del.length-1)])
                rank = u_del.index(" #{ex_type_renc} [#{ex_date}] ")
              end
            end
            rencontre_u << "#{u_del.join("//")}"
        end
        u_fiche = rencontre_u

  
        #on sauvegarde tout

	    @usager.fiche = u_fiche if u_fiche
	    @usager.fiche_jour = u_fiche_jour if u_fiche_jour
	    @usager.save

	    @rencontre.save

	    if TypeRenc.find_by(nom: @rencontre.type_renc).mar?
          m = Maraude.where(date: @rencontre.date, type_maraude: @rencontre.type_renc).first
          @rencontre.update(explo: m.explo)
        else
          @rencontre.update(explo: false)
        end

        if @usager.enfants.any? && @usager.couple
      		flash[:success] = "Rencontre mise à jour avec Famille #{@usager.sexe} #{@usager.nom} #{@usager.prenom} et #{@usager.sexe2} #{@usager.nom2} #{@usager.prenom2}"
     	elsif @usager.enfants.any? && (not @usager.couple)
       		flash[:success] = "Rencontre mise à jour avec Famille #{@usager.sexe} #{@usager.nom} #{@usager.prenom}"
    	elsif (not @usager.enfants.any?) && @usager.couple
       		flash[:success] = "Rencontre mise à jour avec Couple #{@usager.sexe} #{@usager.nom} #{@usager.prenom} et #{@usager.sexe2} #{@usager.nom2} #{@usager.prenom2}"
    	else
      		flash[:success] = "Rencontre mise à jour avec #{@usager.sexe} #{@usager.nom} #{@usager.prenom}"
    	end

        redirect_to "/pqi/#{@rencontre.ville}"

    else
    	if params[:rencontre][:date].blank?
      		flash[:danger] = "Renseignez une date"
      	end
    	if params[:rencontre][:type_renc].empty?
      		flash[:danger] = "Précisez un type de rencontre"
      	end

    	if params[:rencontre][:ville].empty?
      		flash[:danger] = "Précisez une ville de rencontre"
      	end
      	redirect_to "/rencontres/new/#{@usager.id}"
    end
  end

  private

    def rencontre_params
    	@resultat = {}
    	params[:rencontre].each do |t1, t2|
    		@resultat[t1] = t2
    	end

    	@resultat
    end

end
