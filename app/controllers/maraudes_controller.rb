class MaraudesController < ApplicationController

  before_action :logged_in_user,  only: [:new, :create, :index, :show, :edit, :update, :destroy]
  before_action :admin_user,      only: :destroy
  before_action :correct_user,    only: [:show, :edit, :update]

  def index
    if params[:search]
      @maraudes = Maraude.search(params[:search]).paginate(page: params[:page], per_page: 5)
      if @maraudes.empty?
        @maraudes = Maraude.search(params[:search].split('/').reverse.join('-')).paginate(page: params[:page], per_page: 5)
      end
    else
      @maraudes = Maraude.paginate(page: params[:page], per_page: 5)
    end
  end

  def show
    @maraude = Maraude.find(params[:id])
  end

  def new
    @types = []
    unless @current_user.benev?
      TypeRenc.where(mar: true).each do |t|
        @types << ["#{t.nom}"]
      end
    else
      @types << ["Maraude bénévole"]
    end
  end

  def create
    @maraude = Maraude.new(maraude_params)
    @maraude.villes = ""

    if params[:maraude][:date]=="" || params[:maraude][:type_maraude]==""
      flash[:danger] = "Renseignez la date et le type de la maraude"
      redirect_to "/maraudes/new"
    elsif Maraude.find_by(date: params[:maraude][:date].to_date.strftime("%F"), type_maraude: params[:maraude][:type_maraude])
      flash[:danger] = "Cette maraude existe déjà"
      redirect_to "/maraudes/new"
    elsif @maraude.save
      @maraude.date = params[:maraude][:date].to_date.strftime("%F")
      @maraude.save    
      Rencontre.where(date: @maraude.date, type_renc: @maraude.type_maraude).each do |r|
          r.update(explo: @maraude.explo)
      end
      flash[:success] = "Maraude créée"
      redirect_to "/maraudes/edit/#{@maraude.id}"
    else
      flash[:danger] = "Problème inconnu, veuillez réessayer"
      redirect_to "/maraudes/new"
    end
  end

  def edit
    @villes = []
    Ville.where(ville_93: true).order('nom ASC').each do |v|
      @villes << ["#{v.nom}"]
    end
    @maraude = Maraude.find(params[:id])
  end

  def update
    villes = params[:maraude][:villes].reject{ |a| a == '0' }.join("\n")
    compte_rendu = params[:maraude][:cr]
    @maraude = Maraude.find(params[:id])
    
    @maraude.update(villes: villes)
    @maraude.update(cr: compte_rendu)

    nom_des_intervenants = []
    params[:maraude][:intervenants_attributes].each do |t1, t2|
      if t2[:_destroy] == "false"
        nom_des_intervenants << t2[:nom]
        nouvel_interv = Intervenant.find_or_create_by(nom: t2[:nom])
      end
    end
    @maraude.update(intervenants: Intervenant.where(nom: nom_des_intervenants))

    flash[:success] = "Modifications apportées"
    redirect_to "/maraudes/show/#{@maraude.id}"
  end

  def destroy
    Maraude.find(params[:id]).destroy
    flash[:success] = "Maraude supprimée"
    redirect_to maraudes_path
  end

  private

    def correct_user
      if @current_user.benev?
        flash[:danger] = "Accès réservé aux salariés" unless  Maraude.find(params[:id]).type_maraude == "Maraude bénévole"
        redirect_back(fallback_location: root_path) unless  Maraude.find(params[:id]).type_maraude == "Maraude bénévole"
      end
    end

    def maraude_params
      params.require(:maraude).permit(:date,
                                      :type_maraude,
                                      :villes,
                                      :cr,
                                      :explo,
                                      intervenants_attributes: [:nom,
                                                                :ref,
                                                                :_destroy])
    end
end
