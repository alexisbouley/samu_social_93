class StaticController < ApplicationController
	before_action :logged_in_user,  only: [:guide, :listes, :listes_choix, :interv_destroy, :interv_create, :ville_destroy, :ville_create, :type_renc_destroy, :type_renc_create]
  before_action :admin_user,      only: [:ville_destroy, :ville_create, :type_renc_destroy, :type_renc_create]
  before_action :benev_user,      only: [:interv_destroy, :interv_create]

	
  def guide
  end

  def listes
    @choix = session[:choix]
    session.delete(:choix)
  end



    def listes_choix

    	if params["choix_de_liste"] == "rien"
      		flash[:danger] = "Veuillez choisir une liste"
      		session[:choix] = nil 	
    	else
    		session[:choix] = params["choix_de_liste"]
 		end  

    	redirect_to listes_path      		
  	end

  	def interv_create

  		if params[:intervenant][:nom]== ""
  			flash[:danger] = "Problème rencontré : veuillez rentrer le nom de l'intervenant à ajouter" 
  		elsif Intervenant.where(nom: params[:intervenant][:nom]).count > 0
  			flash[:danger] = "Problème rencontré : cet intervenant existe déjà"
  		else
  			Intervenant.create nom: params[:intervenant][:nom]
  			flash[:success] = "Intervenant créé"
  		end

  		session[:choix] = "intervs"
  		redirect_to listes_path
  	end


  	def interv_destroy
  		Intervenant.find(params[:id]).destroy
    	flash[:success] = "Intervenant supprimé"
    	session[:choix] = "intervs"
  		redirect_to listes_path
  	end

  	def ville_create
  		if params[:ville][:nom]== ""
  			flash[:danger] = "Problème rencontré : veuillez rentrer le nom de la ville à ajouter" 
  		elsif Ville.where(nom: params[:ville][:nom]).count > 0
  			flash[:danger] = "Problème rencontré : cette ville existe déjà"
  		else
  			Ville.create nom: params[:ville][:nom], ville_93: params[:ville][:ville_93]
  			flash[:success] = "Ville créé"
  		end

  		session[:choix] = "villes"
  		redirect_to listes_path
  	end

  	def ville_destroy
  		Ville.find(params[:id]).destroy
    	flash[:success] = "Ville supprimée"
    	session[:choix] = "villes"
  		redirect_to listes_path
  	end

  	def type_renc_create
  		if params[:type_renc][:nom]== ""
  			flash[:danger] = "Problème rencontré : veuillez rentrer le nom du type de rencontre à ajouter" 
  		elsif TypeRenc.where(nom: params[:type_renc][:nom]).count > 0
  			flash[:danger] = "Problème rencontré : ce type de rencontre existe déjà"
  		else
  			TypeRenc.create nom: params[:type_renc][:nom], mar: params[:type_renc][:mar]
  			flash[:success] = "Type de rencontre créé"
  		end

  		session[:choix] = "types"
  		redirect_to listes_path
  	end

  	def type_renc_destroy
  		TypeRenc.find(params[:id]).destroy
    	flash[:success] = "Type de rencontre supprimé"
    	session[:choix] = "types"
  		redirect_to listes_path
  	end

end
