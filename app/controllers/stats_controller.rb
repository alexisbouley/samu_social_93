class StatsController < ApplicationController
  before_action :logged_in_user,  only: [:show, :create, :index]
  before_action :benev_user,      only: [:show, :create, :index]

  def show

    @ville = nil
    @dates = nil
    @type = nil

    @villes = []
    Ville.where(ville_93: true).order('nom ASC').each do |v|
      @villes << ["#{v.nom}"]
    end

    @types = []
    TypeRenc.all.order('nom ASC').each do |t|
      @types << ["#{t.nom}"]
    end

  end

  def create
    if !params[:stat][:date_deb].present? && !params[:stat][:date_fin].present?
      flash[:danger] = "Veuillez indiquer une période !"
      redirect_to stats_path
    elsif !params[:stat][:date_deb].present? && params[:stat][:date_fin].present?
      flash[:danger] = "Veuillez indiquer une date de début de période !"
      redirect_to stats_path
    elsif params[:stat][:date_deb].present? && !params[:stat][:date_fin].present?
      flash[:danger] = "Veuillez indiquer une date de fin de période !"
      redirect_to stats_path
    else

      @ville = params[:stat][:villes].reject{ |a| a == '0' }
      if @ville == []
        Ville.where(ville_93: true).order('nom ASC').each do |v|
          @ville << "#{v.nom}"
        end
      end

      @total=[]
      @ville.each do |vil|
        @total<<vil
      end
      @ville << @total

      @type = params[:stat][:types].reject{ |a| a == '0' }
      if @type == []
        TypeRenc.all.order('nom ASC').each do |t|
          @type << "#{t.nom}"
        end
      end

      if params[:stat][:signale] == "1"
        @signale = true
        @signalement = params[:stat][:signalement].reject{ |a| a == '0' }
        if @signalement == []
          @signalement=["Signalement 115", "Signalement tiers"]
        end
      else
        @signale = false
        @signalement=[]
      end

      if params[:stat][:exploration] == "1"
        @exploration = true
      else
        @exploration = false
      end

      if params[:stat][:nouveau] == "1"
        @nouveau = true
      else
        @nouveau = false
      end

      @dates = [params[:stat][:date_deb], params[:stat][:date_fin]]

      @villes = []
      Ville.where(ville_93: true).order('nom ASC').each do |v|
        @villes << ["#{v.nom}"]
      end

      @types = []
      TypeRenc.all.order('nom ASC').each do |t|
        @types << ["#{t.nom}"]
      end


      session[:stored_ville]= @ville[0, @ville.size-1]
      session[:stored_type]= @type
      session[:stored_dates]= @dates
      session[:stored_signale]= @signale
      session[:stored_signalement]= @signalement
      session[:stored_exploration]= @exploration
      session[:stored_nouveau]= @nouveau

      render "show"
    end
  end

  def index

    if params[:ville] == "total"
      vil = session[:stored_ville]
    else
      vil = params[:ville]
    end

    if session[:stored_exploration] && session[:stored_signale]  
      r = Rencontre.where(explo: true, signale: true, signalement: session[:stored_signalement], prev: false, ville: vil, type_renc: session[:stored_type], date: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))) 
    elsif session[:stored_exploration] && (not session[:stored_signale])
      r = Rencontre.where(explo: true, prev: false, ville: vil, type_renc: session[:stored_type], date: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-'))))
    elsif (not session[:stored_exploration]) && session[:stored_signale] 
      r = Rencontre.where(signale: true, signalement: session[:stored_signalement], prev: false, ville: vil, type_renc: session[:stored_type], date: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-'))))
    else
      r = Rencontre.where(prev:false, ville: vil, type_renc: session[:stored_type], date: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-'))))
    end


    if params[:categorie] == "1" #les ménages rencontrés
      r = r.where(dnv: false, tel: false)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "2" #les ménages non vu sur déplacement
      r = r.where(dnv: true)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "3" #les ménages non vu joint au téléphone
      r = r.where(tel: true)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "4" #les familles rencontrées
      r = r.where(dnv: false, tel: false)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).where(id: Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id")).where(id: Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "5" #les couples rencontrés
      r = r.where(dnv: false, tel: false)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), couple: true, created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).where("id NOT IN(?)", Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id"), couple: true).where("id NOT IN(?)", Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "6" #les hommes/femmes seuls rencontrés
      r = r.where(dnv: false, tel: false)
      if session[:stored_nouveau]
        @usagers = Usager.where(id: r.select("usager_id"), couple: false, created_at: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).where("id NOT IN(?)", Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      else        
        @usagers = Usager.where(id: r.select("usager_id"), couple: false).where("id NOT IN(?)", Enfant.select("usager_id")).paginate(page: params[:page], per_page: 50)
      end

    elsif params[:categorie] == "7" #les demandes d'hébergement 
        @usagers = Usager.where(date_dmde: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).paginate(page: params[:page], per_page: 50)

    elsif params[:categorie] == "8" #les demandes d'hébergement qui ont trouvé une solution         
        @usagers = Usager.where(dmde: false, date_dmde: (Date.parse(session[:stored_dates].first.split('/').reverse.join('-')))..(Date.parse(session[:stored_dates].last.split('/').reverse.join('-')))).paginate(page: params[:page], per_page: 50)
    
    end



    @proposition=["Ordre alphabétique", "Ordre chronologique de modification"]
    @usagers = @usagers.order('nom ASC').order('prenom ASC') 
    render "/usagers/index"
  end
end
