class ApplicationController < ActionController::Base

	before_action :set_current_user
  before_action :set_groupe_check

  private

  def set_groupe_check
    unless (params[:controller] == "rencontres" && (params[:action] == "new" || params[:action] == "create") )
      if session.has_key?('groupe')
        session.delete(:groupe)
        session.delete(:usagers_ids)
        session.delete(:type_renc)
        session.delete(:date)
        session.delete(:ville)
      end
    end
  end

  def set_current_user
    if session[:user_id]
      @current_user = User.find(session[:user_id])
    end
  end

  def logged_in_user
      unless !@current_user.nil?
        flash[:danger] = "Merci de vous connecter."
        redirect_to root_url
      end
    end

    def admin_user
      unless @current_user.admin?
        flash[:danger] = "Accès restreint (privilège administrateur)"
        redirect_back(fallback_location: root_path)
      end
    end

    def benev_user
      if @current_user.benev?
        flash[:danger] = "Accès réservé aux salariés"
        redirect_back(fallback_location: root_path)
      end
    end

end
