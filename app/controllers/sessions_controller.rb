class SessionsController < ApplicationController
  
  before_action :logged_in_user, only: :destroy
  

  def new
    if @current_user
      redirect_to usagers_path
    end
  end

  def check

  	@current_user = User.where(identifiant: params[:session][:identifiant], password_digest: params[:session][:password]).first
    
    if @current_user
    	session[:user_id] = @current_user.id
    	flash[:success] = "Bienvenue #{@current_user.prenom} #{@current_user.nom} !"
      User.all.each do |u|
        if u.date_naissance.present?
          if u.date_naissance.month == Date.today.month && u.date_naissance.day == Date.today.day
            flash[:info] = "C'est l'anniversaire de #{u.prenom} #{u.nom} !"
          end
        end
      end
     	redirect_to usagers_path
    else
    	session[:user_id] = nil
     	flash[:danger] = "Échec de la connexion"
      	redirect_to root_path
    end
  end

  def destroy
    session.delete(:user_id)
    @current_user = nil
    redirect_to root_path 
  end
end
