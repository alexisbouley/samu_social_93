class GroupesController < ApplicationController
  before_action :logged_in_user,  only: [:index, :show, :edit, :update, :destroy]
  before_action :admin_user,      only: :destroy
  before_action :benev_user,      only: [:edit, :update, :destroy]

  def index
    if params[:search]
      @groupes = Groupe.search(params[:search]).order('nom ASC').paginate(page: params[:page], per_page: 20)
    else
      @groupes = Groupe.order('nom ASC').paginate(page: params[:page], per_page: 20)
    end
  end

  def show
    @groupe = Groupe.find(params[:id])
    @usagers = @groupe.usagers.order("nom ASC").order("prenom ASC")
  end

  def edit
    @groupe = Groupe.find(params[:id])
  end

  def update
    @groupe = Groupe.find(params[:id])
    
     if @groupe.update(groupe_params)
        flash[:success] = "Nom de groupe édité"
        redirect_to "/groupes/show/#{@groupe.id}"
     else
        flash[:danger] = "Nom de groupe incorrect ou déjà utilisé"
        redirect_to "/groupes/edit/#{@groupe.id}"
     end

  end

  def destroy
    @groupe = Groupe.find(params[:id])

    if @groupe.usagers.count > 0
      flash[:danger] = "Impossible de supprimer le groupe tant qu'il y a encore des usagers dedans"
      redirect_to "/groupes/show/#{@groupe.id}"
    else
      @groupe.destroy
      flash[:success] = "Groupe supprimé"
      redirect_to groupes_path
    end

  end

  private
    def groupe_params
      params.require(:groupe).permit(:nom)
    end

end
