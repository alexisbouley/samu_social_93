class Enfant < ActiveRecord::Base
  belongs_to :usager

  validate :at_least_one
  
  private

  	def at_least_one
  	  if self.prenom.blank? && self.nom.blank?
  	    self.errors.add(:base, "Please fill at least the name or surname")
  	  end
    end
end
