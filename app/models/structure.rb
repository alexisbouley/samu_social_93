class Structure < ActiveRecord::Base
	has_and_belongs_to_many :usagers

	before_save { self.nom = nom.split(' ').map(&:capitalize).join(' ').split('-').map(&:capitalize).join('-') }

	validates :nom, presence: true, uniqueness: {case_sensitive: false}

	default_scope -> { }

	def self.search(search)
      if search
        where("nom ILIKE ? OR ville ILIKE ? OR adresse ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%") 
      end
    end
end
