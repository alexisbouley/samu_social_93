class User < ActiveRecord::Base
	before_save { nom.upcase! }
  	before_save { self.prenom = prenom.split(' ').map(&:capitalize).join(' ').split('-').map(&:capitalize).join('-') }
  
  	default_scope -> { order(nom: :asc) }

  	validates :nom, 		presence: true,
  							length: {maximum: 25}
  	validates :prenom, 		presence: true,
							length: {maximum: 25}
  	validates :identifiant, 	presence: true,
							length: {maximum: 25, minimum: 4},
							uniqueness: {case_sensitive: false}

	validates :password_digest,		presence: true,
							length: {minimum: 6}

end