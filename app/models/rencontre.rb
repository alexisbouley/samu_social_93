class Rencontre < ActiveRecord::Base
  belongs_to :usager

  validates :usager_id, presence: true,
  						:uniqueness => { :scope => [:date, :type_renc] }

  validates :date, presence: true
  validates :type_renc, presence: true
  validates :ville, presence: true

  	def self.search(search)
      if search
        joins(:usager).where("to_char(rencontres.date, 'DD/MM/YYYY') ILIKE? OR rencontres.type_renc ILIKE? OR usagers.nom ILIKE? OR usagers.prenom ILIKE? OR usagers.sexe ILIKE? OR usagers.nom2 ILIKE? OR usagers.prenom2 ILIKE? OR usagers.sexe2 ILIKE?", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
      end
    end
end
