class Usager < ActiveRecord::Base
  has_many :rencontres, :dependent => :destroy
  has_many :enfants, :dependent => :destroy
  belongs_to :groupe, optional: true
  has_and_belongs_to_many :structures

  accepts_nested_attributes_for :enfants, reject_if: lambda { |a| a[:nom].blank? && a[:prenom].blank? },
                                          allow_destroy: true

  default_scope -> { }

  validates :sexe, presence: true
  validates :ville, presence: true
  validate :at_least_one
  validate :at_least_one2

  def groupe_nom
    groupe.try(:nom)
  end

  def groupe_nom=(nom)
    self.groupe = Groupe.find_or_create_by(nom: nom.split(' ').map(&:capitalize).join(' ').split('-').map(&:capitalize).join('-')) if nom.present?
  end


  def self.search(search)
    if search
      where("usagers.nom ILIKE ? OR usagers.ville ILIKE? OR usagers.prenom ILIKE? OR usagers.adresse ILIKE? OR usagers.adresse_précis ILIKE? OR usagers.sexe ILIKE? OR to_char(usagers.date_naissance, 'DD/MM/YYYY') ILIKE? OR usagers.tel ILIKE? OR usagers.nom2 ILIKE? OR usagers.prenom2 ILIKE? OR usagers.sexe2 ILIKE? OR usagers.tel2 ILIKE? OR to_char(usagers.date_naissance2, 'DD/MM/YYYY') ILIKE? ", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
    end
  end

  private
    def at_least_one
      if self.prenom.blank? && self.nom.blank?
        self.errors.add(:base, "Please fill at least the name or surname")
      end
    end

    def at_least_one2
      if self.couple == true      
        if (self.prenom2.blank? && self.nom2.blank?) || sexe2.blank?
          self.errors.add(:base, "Please fill at least the name or surname")
        end
      end      
    end

end