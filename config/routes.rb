Rails.application.routes.draw do
  

  	root                                'sessions#new'
  	post 'login' 		                =>  'sessions#check'
	  delete 'logout'                 =>  'sessions#destroy'

  	get 'users'                     =>  'users#index'
    get 'users/show/:id'            =>  'users#show'
    get 'signup'                    =>  'users#new'
    post 'signup'                   =>  'users#create'  
    get 'users/edit/:id'            =>  'users#edit'
    post 'users/edit/:id'           =>  'users#update'   
    delete 'users/destroy/:id'      =>  'users#destroy'

    get 'guide'                     =>  'static#guide'
    get 'listes'                    =>  'static#listes'
    post 'listes'                   =>  'static#listes_choix'
    post 'intervenant'              =>  'static#interv_create'
    delete 'intervenant/:id'        =>  'static#interv_destroy'
    post 'ville'                    =>  'static#ville_create'
    delete 'ville/:id'              =>  'static#ville_destroy'
    post 'type_renc'                =>  'static#type_renc_create'
    delete 'type_renc/:id'          =>  'static#type_renc_destroy'

    get 'usagers'                   =>  'usagers#index'
    get 'pqi'		              	    =>  'usagers#pqi'
    get 'pqi/:ville'          	    =>  'usagers#pqi'
	  get 'usagers/new'               =>  'usagers#new'
  	post 'usagers/new'              =>  'usagers#create'
  	get 'usagers/edit_comp/:id'     =>  'usagers#edit_comp'
  	post 'usagers/edit_comp/:id'    =>  'usagers#post_comp'
  	get 'usagers/edit/:id'          =>  'usagers#edit'
  	get 'usagers/fiche/:id'         =>  'usagers#fiche'
    get 'usagers/fiche_jour/:id'    =>  'usagers#fiche_jour'
    post 'usagers/edit/:id'         =>  'usagers#update'
    get 'usagers/:id'               =>  'usagers#show'
    delete 'usagers/:id'            =>  'usagers#destroy'

   
  	get 'groupes'                   =>  'groupes#index'
  	get 'groupes/show/:id'          =>  'groupes#show'
  	get 'groupes/edit/:id'          =>  'groupes#edit'
  	post 'groupes/edit/:id'         =>  'groupes#update'
  	delete 'groupes/edit/:id'       =>  'groupes#destroy'


    get 'maraudes'                  =>  'maraudes#index'
    get 'maraudes/show/:id'         =>  'maraudes#show'
  	get 'maraudes/new'              =>  'maraudes#new'
  	post 'maraudes/new'             =>  'maraudes#create'
  	get 'maraudes/edit/:id'         =>  'maraudes#edit'
  	post 'maraudes/edit/:id'        =>  'maraudes#update'
  	delete 'maraudes/edit/:id'      =>  'maraudes#destroy'

    get 'structures'                =>  'structures#index'
    get 'structures/show/:id'       =>  'structures#show'
    get 'structures/new'            =>  'structures#new'
    post 'structures/new'           =>  'structures#create'
    get 'structures/edit/:id'       =>  'structures#edit'
    post 'structures/edit/:id'      =>  'structures#update'
    delete 'structures/edit/:id'    =>  'structures#destroy'


  	get 'rencontres/new/:id'        =>  'rencontres#new'
    post 'rencontres/new/:id'       =>  'rencontres#create'
	  get 'rencontres/edit_form/:id'  =>  'rencontres#edit_form'
    post 'rencontres/edit_form/:id' =>  'rencontres#post_form'
    get 'rencontres/edit/:id'       =>  'rencontres#edit'
    post 'rencontres/edit/:id'      =>  'rencontres#update'
    get 'rencontres/groupe/:id'     =>  'rencontres#new_groupe'
    post 'rencontres/groupe/:id'    =>  'rencontres#post_groupe'

    get 'stats'                     =>  'stats#show'
    post 'stats'                    =>  'stats#create'
    get 'stats/:ville/:categorie'   => 'stats#index'



end
